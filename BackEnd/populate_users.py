import requests
import json

from app.models import User
from app import db




def create_user(data):
    # write your code here
    user = User(username=data['username'],email=data['email'],password=data['password'])
    user.from_dict(data, new_user=True)
    user.get_token()
    user.get_expiration()
    db.session.add(user)
    db.session.commit()
    
    
    # your code should end above this line 
    return 'User {} created'.format(data['username'])

for i in range(1,35):
    username = 'student' + str(i)
    email = 'student' + str(i) + '@gmail.com'
    password = 'password1'
    print(create_user({'username':username, 'email':email, 'password':password}))

for i in range(1,3):
    username = 'teacher' + str(i)
    email = 'teacher' + str(i) + '@gmail.com'
    password = 'password1'
    print(create_user({'username':username, 'email':email, 'password':password}))