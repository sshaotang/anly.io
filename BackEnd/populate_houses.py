import requests
import json

from app.models import House
from app import db



def create_house(data):
    # write your code here
    house = House(name=data['name'],size=data['size'],target_demographic=data['target_demographic'],residents=data['residents'],cost=data['cost'])
    db.session.add(house)
    db.session.commit()
    
    # your code should end above this line 
    return 'House {} created'.format(data['name'])

names = ['Public Housing', 'Condominium', 'Landed Housing', 'Mixed-use']
sizes = [200,200,200,200]
target_demographics = ['low-income','middle-income','high-income','middle-income']
residents_list = [1000,1000,200,500]
costs = [500,1400,300,117]

for i in range(len(names)):
    name = names[i]
    size = sizes[i]
    target_demographic = target_demographics[i]
    residents = residents_list[i]
    cost = costs[i]
    print(create_house({'name':name, 'size':size, 'target_demographic':target_demographic, 'residents':residents, 'cost':cost}))

