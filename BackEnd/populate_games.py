import requests
import json

from app.models import Game
from app import db



def create_game(data):
    # write your code here
    game = Game(
        name = data['name'],
        description = data['description'],
        time_given_minutes = data['time_given'],
        money_given = data['money_given'],
        objective = data['objective'],
        objective_subject = data['objective_subject'],
        objective_sign = data['objective_sign'],
        objective_condition = data['objective_condition'],
        hasLeaderboard = data['has_leaderboard'],
        ldmu = data['ldmu'])
    db.session.add(game)
    db.session.commit()
    
    
    # your code should end above this line 
    return 'Game {} created'.format(data['name'])

names = ['Housing Crisis Tutorial','Housing Crisis Challenge']
descriptions = ['Build enough houses for the people and make them happy!', 'Build enough houses for the people and make them happy!']
time_given = [5,5]
money_givens = [100000000,50000]
objectives = ['happiness > 1','happiness > 1000000']
objective_subjects = ['happiness','happiness']
objective_signs = ['>','>']
objective_conditions = [1,1000000]
has_leaderboards = [False,True]
ldmus = [0.0,0.1] 

for i in range(len(names)):
    name = names[i]
    description = descriptions[i]
    time_given_minutes = time_given[i]
    money_given = money_givens[i]
    objective = objectives[i]
    objective_subject = objective_subjects[i]
    objective_sign = objective_signs[i]
    objective_condition = objective_conditions[i]
    has_leaderboard = has_leaderboards[i]
    ldmu = ldmus[i]
    print(create_game({
                    'name':name,
                    'description':description,
                    'time_given':time_given_minutes,
                    'money_given':money_given,
                    'objective':objective,
                    'objective_subject':objective_subject,
                    'objective_sign':objective_sign,
                    'objective_condition':objective_condition, 
                    'has_leaderboard':has_leaderboard,
                    'ldmu':ldmu}))
