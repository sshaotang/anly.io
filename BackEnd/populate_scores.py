import requests
import json
import random

from app.models import Score, Analytics, Game, User
from app import db



def create_score(data):
    # write your code here
    score = Score(user_id=data['user'],game_id=data['game'],score=data['score'])
    analytics = Analytics(user_id=data['user'], game_id=data['game'],raw_data=data['raw_data'])
    if data['has_won']:
        try:
            get_user_score = Score.query.filter_by(user_id=data['user'], game_id=data['game']).first()
            user_score = get_user_score.serialize()
            if data['score'] > user_score['score']:
                get_user_score.score = data['score']
        except:
            try:
                game = Game.query.get(data['game'])
                game.users.append(User.query.get(data['user']))
                user = User.query.get(data['user'])
                user.scores.append(score)
                db.session.add(score)
            except Exception as e:
                print('failed')
    else:
        try:
            game = Game.query.get(data['game'])
            game.users.append(User.query.get(data['user']))
        except Exception as e:
                print('failed')
    
    user = User.query.get(data['user'])
    user.analytics.append(analytics)
    
    if len(user.analytics) > 3:
        delete_record = Analytics.query.filter_by(user_id=data['user'], game_id=data['game']).first()
        db.session.delete(delete_record)
    db.session.add(analytics)
    db.session.commit()
    
    # your code should end above this line 
    return 'Score for user {} in game {} created'.format(data['user'],data['game'])

for i in range(1,35):
    user_id = i
    game = 2
    score = random.randint(1739284,32193930821)
    random_has_won = random.randint(0,1)
    has_won = True if random_has_won == 1 else False
    raw_data = {'utilisation_rate':(random.randint(1,100))/100}
    print(create_score({'user':user_id, 'game':game, 'score':score, 'has_won':has_won,'raw_data':raw_data}))

