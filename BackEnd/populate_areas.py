import requests
import json

from app.models import Area
from app import db



def create_area(data):
    # write your code here
    area = Area(name=data['name'])
    db.session.add(area)
    db.session.commit()
    
    
    # your code should end above this line 
    return 'Area {} created'.format(data['name'])

names = ['Cheng San','Chong Boon','Kebun Bahru','Sembawang Hills']

for i in range(len(names)):
    name = names[i]
    print(create_area({'name':name}))