import requests
import json

class DataGov:

    @staticmethod
    def get_subzone_info(subzone):
        url_age = "https://data.gov.sg/api/action/datastore_search?resource_id=ad854cc4-f9a3-4208-a9e5-cb8d7fb0a76c&limit=65534"
        url_income = "https://data.gov.sg/api/action/datastore_search?resource_id=2719aca1-6b37-4f8b-ac3f-a866d32df7c6&limit=958"
        r_age = requests.get(url_age)
        results_age = r_age.json()['result']['records']
        r_income = requests.get(url_income)
        results_income = r_income.json()['result']['records']

        filtered = []
        child = ['0_to_4','5_to_9','10_to_14','15_to_19']
        adult = ['20_to_24','25_to_29','30_to_34','35_to_39','40_to_44','45_to_49','50_to_54','55_to_59','60_to_64']
        elderly = ['65_to_69','70_to_74','75_to_79','80_to_84','85_and_over']
        for result in results_age:
            if result['subzone'] == subzone and result['year'] == '2005':
                filtered.append(result)
        num_child = 0
        num_adult = 0
        num_elderly = 0
        for element in filtered:
            for group in child:
                if element['age_group'] == group:
                    num_child += int(element['resident_count'])
                    break
            for group in adult:
                if element['age_group'] == group:
                    num_adult += int(element['resident_count'])
                    break
            for group in elderly:
                if element['age_group'] == group:
                    num_elderly += int(element['resident_count'])
                    break

        filtered2 = []
        low_income = ['HDB Dwellings - 1- and 2-Room Flats','HDB Dwellings - 3-Room Flats']
        middle_income = ['HDB Dwellings - 4-Room Flats','HDB Dwellings - 5-Room and Executive Flats','Condo- miniums and Other Apartments']
        high_income = ['Landed Properties','Others']
        for result in results_income:
            if result['level_3'] == subzone:
                filtered2.append(result)
        num_low_income = 0
        num_middle_income = 0
        num_high_income = 0
        for element in filtered2:
            for group in low_income:
                if element['level_1'] == group:
                    num_low_income += int(element['value'])
                    break
            for group in middle_income:
                if element['level_1'] == group:
                    num_middle_income += int(element['value'])
                    break
            for group in high_income:
                if element['level_1'] == group:
                    num_high_income += int(element['value'])
                    break
        total_population = num_high_income + num_middle_income + num_low_income
        return {
            'total_population':total_population,
            'ageFilter': {
                'num_child':round(round(num_child/total_population,3)*total_population),
                'child_proportion':round(num_child/total_population,3),
                'num_adult':round(round(num_adult/total_population,3)*total_population),
                'adult_proportion':round(num_adult/total_population,3),
                'num_elderly':round(round(num_elderly/total_population,3)*total_population),
                'elderly_proportion':round(num_elderly/total_population,3)
            },
            'incomeFilter': {
                'high_income':num_high_income,
                'middle_income':num_middle_income,
                'low_income':num_low_income
            }
        }

    @staticmethod
    def get_subzone_info_from_json_file(subzone):
        import os
        with open(os.path.abspath('./data/datagov_age.json'), 'r') as f:
            results_age = json.load(f)
        with open(os.path.abspath('./data/datagov_income.json'), 'r') as g:
            results_income = json.load(g)

        filtered = []
        child = ['0_to_4','5_to_9','10_to_14','15_to_19']
        adult = ['20_to_24','25_to_29','30_to_34','35_to_39','40_to_44','45_to_49','50_to_54','55_to_59','60_to_64']
        elderly = ['65_to_69','70_to_74','75_to_79','80_to_84','85_and_over']
        for result in results_age:
            if result['subzone'] == subzone and result['year'] == '2005':
                filtered.append(result)
        num_child = 0
        num_adult = 0
        num_elderly = 0
        for element in filtered:
            for group in child:
                if element['age_group'] == group:
                    num_child += int(element['resident_count'])
                    break
            for group in adult:
                if element['age_group'] == group:
                    num_adult += int(element['resident_count'])
                    break
            for group in elderly:
                if element['age_group'] == group:
                    num_elderly += int(element['resident_count'])
                    break

        filtered2 = []
        low_income = ['HDB Dwellings - 1- and 2-Room Flats','HDB Dwellings - 3-Room Flats']
        middle_income = ['HDB Dwellings - 4-Room Flats','HDB Dwellings - 5-Room and Executive Flats','Condo- miniums and Other Apartments']
        high_income = ['Landed Properties','Others']
        for result in results_income:
            if result['level_3'] == subzone:
                filtered2.append(result)
        num_low_income = 0
        num_middle_income = 0
        num_high_income = 0
        for element in filtered2:
            for group in low_income:
                if element['level_1'] == group:
                    num_low_income += int(element['value'])
                    break
            for group in middle_income:
                if element['level_1'] == group:
                    num_middle_income += int(element['value'])
                    break
            for group in high_income:
                if element['level_1'] == group:
                    num_high_income += int(element['value'])
                    break
        total_population = num_high_income + num_middle_income + num_low_income
        return {
            'total_population':total_population,
            'ageFilter': {
                'num_child':round(round(num_child/total_population,3)*total_population),
                'child_proportion':round(num_child/total_population,3),
                'num_adult':round(round(num_adult/total_population,3)*total_population),
                'adult_proportion':round(num_adult/total_population,3),
                'num_elderly':round(round(num_elderly/total_population,3)*total_population),
                'elderly_proportion':round(num_elderly/total_population,3)
            },
            'incomeFilter': {
                'high_income':num_high_income,
                'middle_income':num_middle_income,
                'low_income':num_low_income
            }
        }
