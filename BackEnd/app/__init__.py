from config import Config
from flask import Flask

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_cors import CORS

# =============================================================================

app = Flask(__name__)
CORS(app)
login = LoginManager(app)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


# =============================================================================


from app import routes, models, errors

# Flask-admin
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

# =============================================================================


app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
app.secret_key = 'super secret key'
# admin = Admin(app, name='app', template_mode='bootstrap3')
# admin.add_view(ModelView(models.User, db.session))
# admin.add_view(ModelView(models.Game, db.session))
# admin.add_view(ModelView(models.Score, db.session))
# admin.add_view(ModelView(models.Area, db.session))
# admin.add_view(ModelView(models.House, db.session))
# admin.add_view(ModelView(models.Facility, db.session))
# admin.add_view(ModelView(models.Analytics, db.session))


# =============================================================================


# Api Blueprints
from app.api import bp as api_bp
app.register_blueprint(api_bp, url_prefix='/api')
