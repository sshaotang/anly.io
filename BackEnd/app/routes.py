import os
from app import app
from flask import send_file, send_from_directory, redirect, url_for, render_template
from app import db

@app.route('/<path:filename>')
def serve_static(filename):
    root_dir = os.path.dirname(os.getcwd())
    print(root_dir)
    return send_from_directory(os.path.join(root_dir, 'BackEnd', 'app', 'dist'), filename)

@app.route('/landing')
def index():
    return render_template('landing/index.html')

@app.route('/')
def angular_app():
    return send_file('dist/index.html')

@app.errorhandler(404)
def not_found_error(error):
    return send_file('dist/index.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500
