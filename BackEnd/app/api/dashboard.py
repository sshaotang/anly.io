import random
import json

from flask import jsonify
from flask import request
from flask import url_for
from flask import g, abort
from flask import Response


from app import db
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth

from app.models import Game


@bp.route('/dashboardgames', methods=['GET'])
def get_all_games_for_dashboard():
    result = [g.serialize_game_dashboard() for g in Game.query.all()]
    response = jsonify(results=result)
    return response
