import math

from flask import jsonify
from flask import request
from flask import url_for
from flask import g, abort


from app import db
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth

from app.models import Score, User, Game, Analytics, House, Facility

# ==========================================================================================

# RETRIEVE SCORE
# (venv) $ http GET http://localhost:5000/api/scores/1
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"
@bp.route('/scores/<int:id>', methods=['GET'])
def get_score(id):
    result = Score.query.get(id).serialize()
    response = jsonify(result)
    return response

@bp.route('/gamescores/<int:game_id>', methods=['GET'])
def get_all_scores(game_id):
    game = Game.query.get(game_id)
    if game.hasLeaderboard:
        result = Game.query.get(game_id).serialize_scores()
        response = jsonify(results=result)
    else:
        result = False
        response = jsonify(result)
    return response

@bp.route('/gamescores/<int:game_id>/<int:user_id>', methods=['GET'])
def get_score_from_game_and_user(game_id,user_id):
    game = Game.query.get(game_id)
    if game.hasLeaderboard:
        result = Game.query.get(game_id).serialize_user_score(user_id)
        response = jsonify(results=result)
    else:
        result = False
        response = jsonify(result)
    return response

# NEW SCORE
# (venv) $ http POST http://localhost:5000/api/scores username=alice password=dog email=alice@example.com
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"
@bp.route('/score', methods=['POST','PUT'])
def create_score():
    data = request.get_json() or {}
    score = Score(user_id=data['user'],game_id=data['game'],score=data['score'])
    analytics = Analytics(user_id=data['user'], game_id=data['game'],basic_stats=data['basic_stats'],assets=data['assets'])
    
    if data['has_won']:
        try:
            get_user_score = Score.query.filter_by(user_id=data['user'], game_id=data['game']).first()
            user_score = get_user_score.serialize()
            if data['score'] > user_score['score']:
                get_user_score.score = data['score']
        except:
            try:
                game = Game.query.get(data['game'])
                game.users.append(User.query.get(data['user']))
                user = User.query.get(data['user'])
                user.scores.append(score)
                db.session.add(score)
            except Exception as e:
                response = jsonify('Failed, ' + str(e))
                response.status_code = 404
    else:
        try:
            game = Game.query.get(data['game'])
            game.users.append(User.query.get(data['user']))
        except Exception as e:
                response = jsonify('Failed, ' + str(e))
                response.status_code = 404
    
    user = User.query.get(data['user'])
    user.analytics.append(analytics)
    
    if len(user.analytics) > 3:
        delete_record = Analytics.query.filter_by(user_id=data['user'], game_id=data['game']).first()
        db.session.delete(delete_record)
    db.session.add(analytics)
    
    db.session.commit()
    response = jsonify()
    response.status_code = 201
    
    return response

   

@bp.route('/noscore', methods=['DELETE'])
def delete_scores():
    scores = Score.query.all()
    for score in scores:
        db.session.delete(score)
        db.session.commit()
    return jsonify('Scores deleted')
    
    
   