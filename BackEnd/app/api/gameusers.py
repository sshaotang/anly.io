import random
import json

from flask import jsonify
from flask import request
from flask import url_for
from flask import g, abort
from flask import Response


from app import db
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth

from app.models import Game


@bp.route('/gameusers/<int:game_id>', methods=['GET'])
def get_all_users(game_id):
    result = Game.query.get(game_id)
    if result:
        result = result.serialize_users()
    response = jsonify(results=result)
    return response
