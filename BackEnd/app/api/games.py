import random
import json

from flask import jsonify
from flask import request
from flask import url_for
from flask import g, abort
from flask import Response


from app import db
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth

from app.models import Game

# ==========================================================================================

# RETRIEVE GAME
# (venv) $ http GET http://localhost:5000/api/games/1
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"
@bp.route('/games/<int:id>', methods=['GET'])
def get_game(id):
    result = Game.query.get(id).serialize()
    response = jsonify(result)
    return response

@bp.route('/allgames', methods=['GET'])
def get_all_games():
    result = [g.serialize() for g in Game.query.all()]
    response = jsonify(results=result)
    return response

