from flask import jsonify
from flask import request
from flask import url_for
from flask import g, abort


from app import db
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth

from app.models import User

# ==========================================================================================

# RETRIEVE USER
# (venv) $ http GET http://localhost:5000/api/users/1
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"
@bp.route('/users/<int:id>', methods=['GET'])
# @token_auth.login_required
def get_user(id):
    response = jsonify(User.query.get_or_404(id).to_dict())
    # return response.headers.add('Access-Control-Allow-Origin', '*')
    return response


# NEW USER
# (venv) $ http POST http://localhost:5000/api/users username=alice password=dog email=alice@example.com
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"
@bp.route('/users', methods=['POST'])
# @token_auth.login_required
def create_user():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include username, email and password fields')
    if User.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user = User(username=data['username'],email=data['email'],password=data['password'])
    user.from_dict(data, new_user=True)
    user.get_token()
    user.get_expiration()
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_user', id=user.id)
    # return response.headers.add('Access-Control-Allow-Origin', '*')
    return response


# MODIFY USER
# (venv) $ http PUT http://localhost:5000/api/users/2 email=alice@example.com
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"
@bp.route('/users/<int:id>', methods=['PUT'])
# @token_auth.login_required
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    print(data)
    if 'username' in data and data['username'] != user.username and User.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if 'email' in data and data['email'] != user.email and User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user.from_dict(data, new_user=False)
    db.session.commit()
    response = jsonify(user.to_dict())
    # return response.headers.add('Access-Control-Allow-Origin', '*')
    return response
