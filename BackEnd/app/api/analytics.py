import pickle

from flask import jsonify
from flask import request
from flask import url_for
from flask import g, abort


from app import db
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth

from app.models import Analytics, Game, User

# ==========================================================================================

# RETRIEVE SCORE
# (venv) $ http GET http://localhost:5000/api/scores/1
# "Authorization:Bearer hQd1UHo6L7nRywH6qJ+P7ytfwiE9SqE+"


@bp.route('/analytics/<int:game_id>', methods=['GET'])
def get_analytics_from_game(game_id):
    result = Game.query.get(game_id).serialize_analytics()
    response = jsonify(results=result)
    return response


@bp.route('/analytics/<int:game_id>/<int:user_id>', methods=['GET'])
def get_analytics_from_game_and_user(game_id,user_id):
    try:
        result = Game.query.get(game_id).serialize_analytics_user(user_id)
        response = jsonify(results=result)
        return response
    except Exception as e:
        return jsonify('Failed')

@bp.route('/noanalytics', methods=['DELETE'])
def delete_analytics():
    analytics = Analytics.query.all()
    for a in analytics:
        db.session.delete(a)
        db.session.commit()
    return jsonify('Analytics records deleted')
    
    
   