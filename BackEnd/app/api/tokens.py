from flask import jsonify, g
from flask import request
from app import db
from app.api import bp
from app.api.auth import basic_auth
from app.api.auth import token_auth

# ========================================================================

# http --auth <username>:<password> POST http://localhost:5000/api/tokens

@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
    # print(request.authorization)
    token = g.current_user.get_token()
    db.session.commit()
    id = g.current_user.id
    username = g.current_user.username
    response = jsonify({ 'token':token, 'id':id, 'username':username })
    # response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@bp.route('/tokens', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    g.current_user.revoke_token()
    db.session.commit()
    return '', 204
