import base64
from datetime import datetime, timedelta
import os
import requests
import json
import random
import math
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist, pdist
import pandas as pd
import numpy as np

from app import db
from app import login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from flask import url_for

from app.datagov import DataGov as datagov

# ================================================================================

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

user_game_table = db.Table('user_game',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('game_id', db.Integer, db.ForeignKey('game.id'), primary_key=True)
)

class User(UserMixin, db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    games = db.relationship('Game', secondary=user_game_table, lazy=True, back_populates='users')
    scores = db.relationship('Score', back_populates='user', cascade='all', lazy=True, uselist=True)
    analytics = db.relationship('Analytics', back_populates='user', cascade='all', lazy=True, uselist=True)

    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    
    def __init__(self, username, email, password, games=None, scores=None, analytics=None):
        self.username = username
        self.email = email
        self.password_hash = password
        games = [] if games is None else games
        self.games = games
        scores = [] if scores is None else scores
        self.scores = scores
        analytics = [] if analytics is None else analytics
        self.analytics = analytics
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'games_id': [g.id for g in self.games] if self.games else [],
            'games_name': [g.name for g in self.games] if self.games else [],
        }
        if include_email:
            data['email'] = self.email
        return data


    def from_dict(self, data, new_user=False):
        for field in ['username', 'email']:
            if field in data:
                setattr(self, field, data[field])

        if new_user and 'password' in data:
            self.set_password(data['password'])


    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def get_expiration(self):
        if self.token_expiration:
            return self.token_expiration
        return None

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def serialize_analytics(self):
        analytics = [a.serialize() for a in self.analytics if a.game_id==self.id]
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name
                },
                'analytics': analytics
            }
        }
        return result


    def __repr__(self):
        return '<User {}>'.format(self.username)

class Game(db.Model):
    __tablename__ = 'game'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), index=True, unique=True)
    description = db.Column(db.String(120))
    time_given_minutes = db.Column(db.Integer, nullable=False)
    money_given = db.Column(db.Integer, nullable=False)
    objective = db.Column(db.String(50))
    objective_subject = db.Column(db.String(50))
    objective_sign = db.Column(db.String(50))
    objective_condition = db.Column(db.Integer)
    hasLeaderboard = db.Column(db.Boolean, default=True)
    ldmu = db.Column(db.Float)
    scores = db.relationship('Score', back_populates='game', cascade='all', lazy=True, uselist=True)
    analytics = db.relationship('Analytics', back_populates='game', cascade='all', lazy=True, uselist=True)
    users = db.relationship('User', secondary=user_game_table, lazy=True, back_populates='games', cascade='all', uselist=True)

    def __init__(self, name, description, time_given_minutes, money_given, objective, objective_subject, objective_sign, objective_condition, hasLeaderboard, ldmu, scores=None, analytics=None, users=None):
        self.name = name
        self.description = description
        self.time_given_minutes = time_given_minutes
        self.money_given = money_given
        self.objective = objective
        self.objective_subject = objective_subject
        self.objective_sign = objective_sign
        self.objective_condition = objective_condition
        self.hasLeaderboard = hasLeaderboard
        self.ldmu = ldmu
        scores = [] if scores is None else scores
        self.scores = scores
        analytics = [] if analytics is None else analytics
        self.analytics = analytics
        users = [] if users is None else users
        self.users = users

    def serialize(self):
        num_areas = len([a.serialize() for a in Area.query.all()])
        # area_id = random.randint(1,num_areas)
        area_id = 1
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name,
                    'description':self.description,
                    'time_given':self.time_given_minutes,
                    'money_given':self.money_given,
                    'objective':self.objective,
                    'objective_subject':self.objective_subject,
                    'objective_sign':self.objective_sign,
                    'objective_condition':self.objective_condition, 
                    'has_leaderboard':self.hasLeaderboard,
                    'ldmu':self.ldmu
                },
                'area': Area.query.get(area_id).serialize(),
                'assets': {
                    'houses': [h.serialize() for h in House.query.all()],
                    'facilities': [f.serialize() for f in Facility.query.all()]
                },
                'users': [u.id for u in self.users]
            }
        }
        return result

    def serialize_scores(self):
        scores = [s.serialize() for s in self.scores if s.game_id==self.id]
        scores.sort(key = lambda s: s['score'], reverse=True)
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name
                },
                'scores': scores
            }
        }
        return result

    def serialize_score(self,score_id):
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name
                },
                'scores': [s.serialize() for s in self.scores if s.id==score_id]
            }
        }
        return result

    def serialize_users(self):
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name
                },
                'users': [{'id':u.id,'username':u.username} for u in self.users]
            }
        }    
        return result

    def serialize_user_score(self, user_id):
        if User.query.get(user_id) in self.users:
            result = {
                'game': {
                    'attributes': {
                        'id':self.id,
                        'name':self.name
                    },
                    'scores': [s.serialize() for s in self.scores if s.user_id==user_id]
                }
            }    
            return result
        return False

    def serialize_game_dashboard(self):
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name,
                    'has_leaderboard':self.hasLeaderboard
                },
                'users': [{'id':u.id,'username':u.username} for u in self.users]
            }
        }
        return result

    def serialize_analytics(self):
        analytics = [a.serialize() for a in self.analytics if a.game_id==self.id]
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name
                },
                'analytics': analytics
            }
        }
        return result

    def serialize_analytics_user(self, user_id):
        analytics = [a.serialize() for a in self.analytics if a.game_id==self.id and a.user_id==user_id]
        result = {
            'game': {
                'attributes': {
                    'id':self.id,
                    'name':self.name
                },
                'analytics': analytics
            }
        }
        return result

class Score(db.Model):
    __tablename__ = 'score'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', back_populates='scores')
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), nullable=False)
    game = db.relationship('Game', back_populates='scores')
    score = db.Column(db.Integer, nullable=False)
    url = db.Column(db.String(100))

    def serialize(self):
        result = {
            'id':self.id, 
            'user_id':self.user_id, 
            'username':User.query.get(self.user_id).username,
            'game':self.game_id, 
            'score':self.score,
            'url':self.url
            }
        return result

class Area(db.Model):
    __tablename__ = 'area'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), index=True, unique=True)

    def serialize(self):
        result = {
            'id':self.id,
            'name':self.name.upper(),
            'population': datagov.get_subzone_info_from_json_file(self.name)
        }
        return result

class House(db.Model):
    __tablename__ = 'house'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), nullable=False)
    size = db.Column(db.Integer, nullable=False)
    target_demographic = db.Column(db.String(120))
    residents = db.Column(db.Integer, nullable=False)
    cost = db.Column(db.Integer, nullable=False)

    def __init__(self, name, size, target_demographic, residents, cost):
        self.name = name
        self.size = size
        self.target_demographic = target_demographic
        self.residents = residents
        self.cost = cost

    def serialize(self):
        result = {
            'id':self.id,
            'name':self.name,
            'type':'House',
            'size':self.size,
            'target_demographic':self.target_demographic,
            'residents':self.residents,
            'cost':self.cost,
            }
        return result

class Facility(db.Model):
    __tablename__ = 'facility'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), nullable=False)
    size = db.Column(db.Integer, nullable=False)
    cost = db.Column(db.Integer, nullable=False)
    buffer = db.Column(db.Integer, nullable=False)
    happiness = db.Column(db.Integer, nullable=False)
    elderly_multiplier = db.Column(db.Float, nullable=False)
    adult_multiplier = db.Column(db.Float, nullable=False)
    child_multiplier = db.Column(db.Float, nullable=False)

    def __init__(self, name, size, cost, buffer, happiness, elderly_multiplier, adult_multiplier, child_multiplier):
        self.name = name
        self.size = size
        self.cost = cost
        self.buffer = buffer
        self.happiness = happiness
        self.elderly_multiplier = elderly_multiplier
        self.adult_multiplier = adult_multiplier
        self.child_multiplier = child_multiplier

    def serialize(self):
        result = {
            'id':self.id,
            'name':self.name,
            'type':'Facility',
            'size':self.size,
            'cost':self.cost,
            'buffer':self.buffer,
            'happiness':self.happiness,
            'multipliers': {
                'elderly':self.elderly_multiplier,
                'adult':self.adult_multiplier,
                'child':self.child_multiplier
            }
        }
        return result

class Analytics(db.Model):
    __tablename__ = 'analytics'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', back_populates='analytics')
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), nullable=False)
    game = db.relationship('Game', back_populates='analytics')
    basic_stats = db.Column(db.PickleType)
    assets = db.Column(db.PickleType)

    def calculate_euclidean_distance(self,point1,point2):
        R = 6373.0
        lat1 = point1[0]
        lon1 = point1[1]
        lat2 = point2[0]
        lon2 = point2[1]
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        x = dlat
        y = dlon * math.cos((lat2 + lat1)*0.00872664626)  
        return 111.319 * math.sqrt(x*x + y*y)
    
    def calculate_happiness_per_budget_used(self,happiness,budget_used):
        return happiness/budget_used
    
    def is_house_inside_facility_buffer(self,house,facility,buffer):
        euclidean_distance = self.calculate_euclidean_distance(house,facility)
        if euclidean_distance > buffer:
            return False
        return True

    def num_residents_served_by_facility(self,facility):
        residents = {'total':0,'high-income':0,'middle-income':0,'low-income':0}
        facility_buffer = Facility.query.filter_by(name=facility['name']).first().buffer
        facility_pos = facility['pos']
        houses = self.assets['houses']
        for house in houses.keys():
            house_obj = House.query.filter_by(name=houses[house]['name']).first()
            income_group = house_obj.target_demographic
            num_residents = house_obj.residents
            for i in range(houses[house]['num']):
                temp_house_pos = houses[house]['pos'][i]
                if self.is_house_inside_facility_buffer(temp_house_pos,facility_pos,facility_buffer):
                    residents[income_group] += num_residents
        residents['total'] += residents['high-income'] + residents['middle-income'] + residents['low-income']
        return residents

    def total_access_to_facilities(self):
        result = {}
        facilities = self.assets['facilities']
        for facility in facilities.keys():
            facility_obj = Facility.query.filter_by(name=facilities[facility]['name']).first()
            total_residents_served = 0
            for i in range(facilities[facility]['num']):
                temp_facility = {}
                temp_facility['name'] = facilities[facility]['name']
                temp_facility['pos'] = facilities[facility]['pos'][i]
                total_residents_served = self.num_residents_served_by_facility(temp_facility)['total']
            result[facilities[facility]['name']] = total_residents_served
        return result

    def find_optimal_k(self,X):
        from sklearn.metrics import silhouette_score

        sil = []
        KMAX = 10

        # dissimilarity would not be defined for a single cluster, thus, minimum number of clusters should be 2
        for k in range(2, KMAX+1):
            kmeans = KMeans(n_clusters = k).fit(X)
            labels = kmeans.labels_
            sil.append(silhouette_score(X, labels, metric = 'euclidean'))
        
        sil_max = max(sil)
        
        len_sil = len(sil)
        for i in range(len_sil):
            rounded_sil = round(sil[i], 1)
            rounded_sil_max = round(sil_max, 1)
            if (rounded_sil == rounded_sil_max) and (sil[i] > sil[i+1]):
                return i+2

    def geoclustering_k_means(self,list_of_points,k):
        df = pd.DataFrame(list_of_points)
        df = df.rename({0: 'Lat', 1: 'Long'}, axis=1)
        X = df[['Lat', 'Long']].values
        kmean = KMeans(n_clusters=k).fit(X)
        return kmean

    def calculate_social_mixing(self,clusters):
        return 0
    
    def analyse_clusters(self):
        try:
            analysis = {}
            houses = self.assets['houses']
            list_of_points = []
            num_of_houses_by_type = []
            for house in houses.keys():
                instances_of_house = houses[house]['pos']
                for instance in instances_of_house:
                    list_of_points.append(instance)
                num_of_houses_by_type.append(houses[house]['num'])
            optimal_k = self.find_optimal_k(np.array(list_of_points))
            analysis['number_of_clusters'] = optimal_k
            clusters = self.geoclustering_k_means(list_of_points,optimal_k)
            cluster_labels = clusters.labels_
            houses_grouped_by_cluster = [{'cluster':[]} for i in range(optimal_k)]
            counter_total = 0
            house_types = list(houses.keys())
            for i in range(len(num_of_houses_by_type)):
                counter_num_houses = 0
                for label in cluster_labels[counter_total:counter_total+num_of_houses_by_type[i]]:
                    label = cluster_labels[counter_total]
                    counter_total += 1
                    target_house = houses[house_types[i]]['pos'][counter_num_houses]
                    houses_grouped_by_cluster[label]['cluster'].append({"name":house_types[i],"pos":target_house})
                    counter_num_houses += 1
                    if counter_num_houses > counter_total:
                        counter_num_houses = 0
            for cluster in houses_grouped_by_cluster:
                for house_type in house_types:
                    cluster['num_'+house_type] = sum([1 for c in cluster['cluster'] if house_type in c['name']])
            analysis['social_mixing'] = houses_grouped_by_cluster
            return analysis
        except Exception as e:
            return "Cluster Analysis Failed"
        

    def age_group_proportion_of_happiness(self):
        facilities = self.assets['facilities']
        num_each_facility = {}
        happiness_age_group = {'total':0, 'child':0, 'adult':0, 'elderly':0}
        for facility in facilities.keys():
            num_each_facility[facility] = facilities[facility]['num']
            facility_obj = Facility.query.filter_by(name=facilities[facility]['name']).first()
            base_happiness = facility_obj.happiness
            child_multiplier = facility_obj.child_multiplier
            adult_multiplier = facility_obj.adult_multiplier
            elderly_multiplier = facility_obj.elderly_multiplier
            total_multiplier = child_multiplier + adult_multiplier + elderly_multiplier
            happiness_age_group['child'] += base_happiness*child_multiplier
            happiness_age_group['adult'] += base_happiness*adult_multiplier
            happiness_age_group['elderly'] += base_happiness*elderly_multiplier
            happiness_age_group['total'] += base_happiness*total_multiplier
        happiness_age_group['child'] = happiness_age_group['child']/happiness_age_group['total']
        happiness_age_group['adult'] = happiness_age_group['adult']/happiness_age_group['total']
        happiness_age_group['elderly'] = happiness_age_group['elderly']/happiness_age_group['total']
        return happiness_age_group

    def calculate_happiness_gini_coefficient(self,population_dist):
        population_distribution = {}
        population_distribution['child'] = round(population_dist['child_proportion']*100)
        population_distribution['adult'] = round(population_dist['adult_proportion']*100)
        population_distribution['elderly'] = round(population_dist['elderly_proportion']*100)
        happiness_age_group = self.age_group_proportion_of_happiness()
        del happiness_age_group['total']
        age_groups = happiness_age_group.keys()
        list_of_values = []
        for age_group in age_groups:
            value = happiness_age_group[age_group]
            num_of_values = population_distribution[age_group]
            for i in range(num_of_values):
                list_of_values.append(value)
        sorted_list = sorted(list_of_values)
        height, area = 0, 0
        for value in sorted_list:
            height += value
            area += height - value / 2.
        fair_area = height * len(list_of_values) / 2.
        return (fair_area - area) / fair_area


    def efficiency(self,game):
        result = {}
        happiness = self.basic_stats['happiness']
        budget_used = self.basic_stats['budget_used']
        happiness_per_budget_used = self.calculate_happiness_per_budget_used(happiness,budget_used)
        result['happiness_per_budget_used'] = round(happiness_per_budget_used)
        budget_wastage = budget_used/game.money_given
        result['proportion_of_budget_wasted'] = budget_wastage
        result['overall_efficiency'] = round(happiness_per_budget_used*(1-budget_wastage))
        return result

    def effectiveness(self,game):
        result = {}
        total_access_to_facilities = self.total_access_to_facilities()
        game_area = game.serialize()['game']['area']
        population = game_area['population']['total_population']
        for facility in total_access_to_facilities.keys():
            proportion_population_served = total_access_to_facilities[facility]/population
            result['proportion_of_population_served_by_' + facility.lower()] = proportion_population_served if proportion_population_served < 1.0 else 1.0
        result['overall_effectiveness'] = sum(result.values())/len(result.keys())
        return result

    def equity(self,game):
        result = {}
        game_area = game.serialize()['game']['area']
        population = game_area['population']['ageFilter']
        gini_coefficient_age = self.calculate_happiness_gini_coefficient(population)
        result['gini_coefficient_happiness_across_age_groups'] = gini_coefficient_age
        result['cluster_analysis'] = self.analyse_clusters()
        return result
    
    def analysis(self):
        analysis = {'efficiency':{},'effectiveness':{},'equity':{}}
        game = Game.query.get(self.game_id)
        analysis['efficiency'] = self.efficiency(game)
        analysis['effectiveness'] = self.effectiveness(game)
        analysis['equity'] = self.equity(game)
        analysis['has_won'] = self.basic_stats['has_won']
        analysis['score'] = self.basic_stats['happiness']
        return analysis
    
    def serialize(self):
        result = {
            'id':self.id, 
            'user_id':self.user_id, 
            'username':User.query.get(self.user_id).username,
            'game':self.game_id, 
            'analysis':self.analysis()
        }
        return result
