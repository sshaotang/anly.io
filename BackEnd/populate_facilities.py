import requests
import json

from app.models import Facility
from app import db



def create_facility(data):
    # write your code here
    facility = Facility(
        name=data['name'],
        size=data['size'],
        buffer=data['buffer'],
        happiness=data['happiness'],
        cost=data['cost'],
        elderly_multiplier=data['multipliers']['elderly'],
        adult_multiplier=data['multipliers']['adult'],
        child_multiplier=data['multipliers']['child']
    )
    db.session.add(facility)
    db.session.commit()   
    
    # your code should end above this line 
    return 'Facility {} created'.format(data['name'])

names = ['Clinic', 'Gas Station', 'Mall','Playground','School','Community Centre','Food Centre','Market','Elderly Care Centre']
sizes = [10,40,200,1,300,200,200,200,100]
buffers = [100,350,300,50,300,225,105,105,90]
happiness_list = [183,17,117,25,249,66,145,154,44]
costs = [100,50,1200,20,800,400,200,150,300]
elderly_multipliers = [6.0,1.0,4.0,3.0,0.0,10.0,10.0,13.0,25.0]
adult_multipliers = [3.0,4.5,10.0,1.0,3.0,3.0,10.0,7.0,0.0]
child_multipliers = [3.0,0.0,4.0,20.0,10.0,5.0,2.0,0.0,0.0]

for i in range(len(names)):
    name = names[i]
    size = sizes[i]
    buffer = buffers[i]
    happiness = happiness_list[i]
    cost = costs[i]
    elderly_multiplier = elderly_multipliers[i]
    adult_multiplier = adult_multipliers[i]
    child_multiplier = child_multipliers[i]

    print(create_facility({
            'name':name,
            'size':size,
            'cost':cost,
            'buffer':buffer,
            'happiness':happiness,
            'multipliers': {
                'elderly':elderly_multiplier,
                'adult':adult_multiplier,
                'child':child_multiplier}}))


