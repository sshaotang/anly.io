import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GisgameComponent } from './gisgame.component';

describe('GisgameComponent', () => {
  let component: GisgameComponent;
  let fixture: ComponentFixture<GisgameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GisgameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GisgameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
