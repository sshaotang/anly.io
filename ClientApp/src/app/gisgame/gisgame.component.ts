import { Component, OnInit, NgZone, ViewContainerRef, ComponentRef } from '@angular/core';
import { GisgameService } from '../services/gisgame/gisgame.service';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Router, NavigationStart } from '@angular/router';
import { timer } from 'rxjs';
import { latLng, latLngBounds, Map, icon, Layer } from 'leaflet';
import { stringify } from 'querystring';
declare let L;
declare var $: any;


@Component({
  selector: 'app-gisgame',
  templateUrl: './gisgame.component.html',
  styleUrls: ['./gisgame.component.css']
})
export class GisgameComponent implements OnInit {


    // GEO CLASSES
    private cMarker = L.Marker.extend({ options: { id: null, asset: null }});
    private cCircle = L.Circle.extend({ options: { id: null, asset: null }});

    // VISUALS (PRE GAME)
    public showIntro = true;
    public showModal = false;

    // VISUALS (IN GAME)
    public objectiveOpen = false;
    public popDetailOpen = false;
    public isHousesTab = true;

    // VISUALS (POST GAME)
    public isGameOver = false;
    public fulfillWin = false;
    public endingMsg = '';

    // AUDIO
    public canPlayAudio = true;
    public crowdAudio = null;
    public bgmAudio = null;

    // LOGIC STATE MANAGEMENT
    public currentUser = null;
    public timer = {
      timeLeft: 0,
      subscribedVal: ''
    };

    public state = {
      game: {
        currPopulation: 0,
        currScore: 0,
        currScoreChange: '0',
        currMoney: 0,
        currIncomeFilter: {
          lowIncome: 0,
          middleIncome: 0,
          highIncome: 0,
        },
      },
      track: { id: 0 },
      storage: {
        markers: [],
        circles: [],
        houses: [],
      },
      action: {
        targetAsset: null,
        dragging: false,
        dropEvent: null,
        mId: null,
        mAsset: null,
        mPrevPos: null
      },
      elements: {
        map: null,
        mapElement: null,
      },
      subzone: { layer: null, geometry: null }
    };

  // ============================================================================================================================

  streetMaps = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-background/{z}/{x}/{y}{r}.png', { detectRetina: true, opacity: 0.1 });
  layersControl = { baseLayers: { 'Street Maps': this.streetMaps, }, overlays: {} };
  options = { zoom: 15, center: latLng([ 1.37, 104 ]), zoomControl: false, 
    layers: [this.streetMaps],
  };

  // ============================================================================================================================

  constructor(
    private http: HttpClient,
    public gisgameService: GisgameService,
    public authenticationService: AuthenticationService,
    private matSnackBar: MatSnackBar,
    public viewRef: ViewContainerRef,
    public router: Router,
  ) {
    if (this.gisgameService.getGisgameData() == null) {
      this.router.navigate(['/dashboard']);
    } else {
      this.currentUser = authenticationService.currentUserValue;
      router.events.subscribe((val) => {
        if (val instanceof NavigationStart)  {
          if (this.crowdAudio instanceof Audio) { this.crowdAudio.pause(); this.crowdAudio = null; }
          if (this.bgmAudio instanceof Audio) { this.bgmAudio.pause(); this.bgmAudio = null; }
        }
      });
    }
  }

  ngOnInit() {}

  /***************************
     HIGHER ORDER
  ****************************/
  public onMapReady(map: Map): void {
    this.state.elements.map = map;
    this.state.elements.mapElement = document.getElementById('map');
    map.on('dragend', () => {
      if (this.state.action.dragging === false && this.state.action.dropEvent == null) { map.dragging.disable(); }
    });
    map.on('click', (e) => { this.mapOnClick(e, map); });
    this.loadGame();
  }

  public loadGame(): void {
      this.state.game.currMoney = this.gisgameService.getAttributes().money_given;
      this.generateSubzone(this.state.elements.map);
  }

  public startGame(): void {
      this.startTimer();
      this.startAudio();
      this.showIntro = false;
  }

  private startTimer(): void {
    this.timer.timeLeft = this.gisgameService.getAttributes().time_given * 60; // IN SECONDS
    // this.timer.timeLeft = 10; // testing
    const timerObj = timer(500, 1000); // Delay for 50ms before starting, 1 second interval
    const sub = timerObj.subscribe(val => {
      const remainSeconds: number = this.timer.timeLeft - val;
      if (remainSeconds < 0) { sub.unsubscribe(); this.gameOver(); } else { this.timer.subscribedVal = this.displayTime(remainSeconds); }
    });
  }

  private generateSubzone(map): void {
    const subzoneName = this.gisgameService.getArea().name;
    this.http.get('assets/gisgame/geojson/subzone/' + subzoneName + '.geojson').subscribe((geojson: any) => {
      const szGroup = L.geoJSON(geojson, {style: { color: '#bcbcbc' , weight: 0, fillOpacity: 0.95 }});
      szGroup.addTo(map);
      this.state.subzone.layer = szGroup._layers[Object.keys(szGroup._layers)[0]];
      this.state.subzone.geometry = this.state.subzone.layer.feature.geometry;
      map.panTo(latLng(
        (this.state.subzone.layer._bounds._northEast.lat + this.state.subzone.layer._bounds._southWest.lat) / 2,
        (this.state.subzone.layer._bounds._northEast.lng + this.state.subzone.layer._bounds._southWest.lng) / 2)
      );
      const ob = this.state.subzone.layer._bounds;
      const nb = latLngBounds(
        latLng(ob._southWest.lat - 0.005, ob._southWest.lng - 0.005),
        latLng(ob._northEast.lat + 0.005, ob._northEast.lng + 0.005)
      );
      map.setMaxBounds(nb);
    });
  }

  /***************************
     INTERACTIVE
  ****************************/

  public dragStart(event): void { this.playSoundEffect("pickup"); this.state.action.dragging = true; }

  public dragEnd(event, asset): void {
    this.state.action.dropEvent = event;
    this.state.action.targetAsset = asset;
    try { this.state.elements.mapElement.click(); } catch (e) {}
    this.state.action.dropEvent = null;
    this.state.action.dragging = false;
  }

  private mapOnClick(e, map): void {
    map.dragging.enable();
    const asset = this.state.action.targetAsset;

    if (asset) {

      if (this.state.game.currMoney < asset.cost) {
        this.playSoundEffect("error");
        this.setStatus('Not enough money!');
      } else if (this.state.action.dragging === true && this.state.action.dropEvent != null && this.state.game.currMoney >= asset.cost) {

        const newLatlng = map.mouseEventToLatLng(this.state.action.dropEvent);

        if (this.isLatlngInsideSubzone(newLatlng) === false) { this.playSoundEffect("error"); this.setStatus('Out of bounds!'); } else {

          if (asset.type === 'Facility') {
            const newCircle = new this.cCircle([ newLatlng.lat, newLatlng.lng ], {
              id: this.state.track.id, asset, radius: asset.buffer
            }).setStyle({ weight: 0.5, color: '#04c5cd', fillOpacity: 0.1});
            const newMarker = this.newMarkerBuilder(asset, newLatlng, map);

            // check buffer intersection
            if (this.isFacilityBufferIntersectSame(newCircle) === true) { this.playSoundEffect("error"); this.setStatus('Buffers of similar facilities cannot overlap!'); }
            else {
              this.state.storage.markers.push(newMarker);
              if (newMarker instanceof Layer) { map.addLayer(newMarker); }
              this.state.storage.circles.push(newCircle);
              map.addLayer(newCircle);
              this.state.track.id++;
              this.state.game.currMoney -= asset.cost;
              this.playSoundEffect("place");
              this.checkHousesInCircle();
            }

          } else if (asset.type === 'House') {

            const newMarker = this.newMarkerBuilder(asset, newLatlng, map);
            this.state.storage.markers.push(newMarker);
            if (newMarker instanceof Layer) { map.addLayer(newMarker); }

            switch (asset.target_demographic) {
              case "high-income":
                this.state.game.currIncomeFilter.highIncome += asset.residents;
                break;
              case "middle-income":
                this.state.game.currIncomeFilter.middleIncome += asset.residents;
                break;
              case "low-income":
                this.state.game.currIncomeFilter.lowIncome += asset.residents;
            }
            this.state.game.currPopulation += asset.residents;
            this.state.storage.houses.push(newMarker);
            this.state.track.id++;
            this.state.game.currMoney -= asset.cost;

            this.playSoundEffect("place");
            this.changeCrowdVolume();
            this.checkHousesInCircle();
          }}}}

    this.state.action.dropEvent = null;
    this.state.action.targetAsset = null;
  }

  private newMarkerBuilder(asset, newLatlng, map): object {
    let tooltipText = asset.name + ' (' + asset.type + ')<br>';
    if (asset.type === 'Facility') { tooltipText += 'Buffer Radius: ' + asset.buffer + '<br>Happiness: ' + asset.happiness;
    } else { tooltipText += 'Residents: ' + asset.residents; }

    const newMarker = new this.cMarker([ newLatlng.lat, newLatlng.lng ], {
      id: this.state.track.id,
      asset,
      draggable: true,
      icon: icon({
        iconSize: [ 30, 30 ],
        iconUrl: 'assets/gisgame/assets-svg/' + asset.name + '.svg',
      })})
    .on('click', this.onClick.bind(this)).bindTooltip(tooltipText, { sticky: false, opacity: 0.6 })
    .on('dragstart', (e) => { this.markerDragStart(e, map); })
    .on('dragend', (e) => { this.markerDragEnd(e, map); });

    return newMarker;
  }

  private markerDragStart(e, map): void {
    if (this.state.action.mId == null && this.state.action.mPrevPos == null) {
      this.playSoundEffect("pickup");
      this.state.action.mId = e.target.options.id;
      let circleToRemove = null;
      this.state.storage.circles.forEach((circle) => {
        if (circle.options.id === this.state.action.mId) {
          circleToRemove = circle;
          this.state.action.mAsset = circle.options.asset;
          this.state.action.mPrevPos = circle.getLatLng();
      }});
      if (this.state.action.mAsset != null) {
        map.removeLayer(circleToRemove);
        this.state.storage.circles = this.state.storage.circles.filter((circleToRemove) =>
          circleToRemove.options.id !== this.state.action.mId);
      }}
  }

  private markerDragEnd(e, map): void {
    this.playSoundEffect("place");
    if (this.state.action.mId != null) {
      if (this.isLatlngInsideSubzone(e.target._latlng) === false) {
        // marker was dragged out of subzone, discard
        this.state.storage.markers.forEach((marker) => {
          if (marker.options.id === this.state.action.mId) {
            if (marker.options.asset.type === 'House') {
              this.state.storage.houses = this.state.storage.houses.filter((house) => house.options.id !== marker.options.id);
              switch (marker.options.asset.target_demographic) {
                case "high-income":
                  this.state.game.currIncomeFilter.highIncome -= marker.options.asset.residents;
                  break;
                case "middle-income":
                  this.state.game.currIncomeFilter.middleIncome -= marker.options.asset.residents;
                  break;
                case "low-income":
                  this.state.game.currIncomeFilter.lowIncome -= marker.options.asset.residents;
              }
              this.state.game.currPopulation -= marker.options.asset.residents;
            }
            this.state.game.currMoney += marker.options.asset.cost;
            map.removeLayer(marker);
            this.playSoundEffect("destroy");
            this.changeCrowdVolume();
            this.setStatus(marker.options.asset.name + ' was demolished!');
          }});
        this.state.storage.markers = this.state.storage.markers.filter((marker) => marker.options.id !== this.state.action.mId);
      } else {
        // circle was found during marker drag start, re-render
        if (this.state.action.mAsset != null) {
          const mll = e.target._latlng;
          const followCircle = new this.cCircle([ mll.lat, mll.lng ], {
            id: this.state.action.mId,
            asset: this.state.action.mAsset,
            radius: this.state.action.mAsset.buffer
          }).setStyle({ weight: 0.5, color: '#04c5cd', fillOpacity: 0.1});
          if (this.state.action.mAsset.type === "Facility" && this.isFacilityBufferIntersectSame(followCircle) === true) {
            this.playSoundEffect("error");
            this.setStatus('Buffers of similar facilities cannot overlap!');
            // revert to original position
            this.state.storage.markers.forEach((marker) => {
              if (marker.options.id === this.state.action.mId) {
                marker.setLatLng(this.state.action.mPrevPos).update();
                followCircle.setLatLng(this.state.action.mPrevPos);
              }});
          }
            map.addLayer(followCircle);
            this.state.storage.circles.push(followCircle);
        }}
      this.checkHousesInCircle();
    }
    this.state.action.mId = null;
    this.state.action.mAsset = null;
    this.state.action.mPrevPos = null;
  }

  private onClick(marker) {}

  private checkHousesInCircle(): void {
    let newScore = 0;
    // this.state.game.currScore = 0;
    const ageFiltered = this.gisgameService.getArea().population.ageFilter;
    const scope = this;
    this.state.storage.circles.forEach((circle) => {
      const happiness = circle.options.asset.happiness;
      const multipliers = circle.options.asset.multipliers;

      let ldmuFactor = 0.75; // TEST, to replace from models
      let ldmuMultiplier = 1; // LDMU

      scope.state.storage.houses.forEach((house) => {
        const proximity = scope.isMarkerInCircle(circle, house);
        if (proximity !== -1) { 
          const residents = house.options.asset.residents;
          const proportions = {
            adult: residents * ageFiltered.adult_proportion,
            child: residents * ageFiltered.child_proportion,
            elderly: residents * ageFiltered.elderly_proportion
          };
          newScore += this.calculateScore(happiness, proportions, multipliers, proximity, ldmuMultiplier);
          ldmuMultiplier *= ldmuFactor; // Implement LDMU
        }
      }); 
    });
    // Change in Score
    if (newScore > this.state.game.currScore) {
      this.state.game.currScoreChange = '+' + (newScore - this.state.game.currScore).toString();
      // this.setStatus(this.state.game.currScoreChange + " 🙂");
    } else if (newScore < this.state.game.currScore) {
      this.state.game.currScoreChange = (newScore - this.state.game.currScore).toString();
      // this.setStatus(this.state.game.currScoreChange + " 🙂");
    }
    this.state.game.currScore = newScore;
  }

  private isMarkerInCircle(circle, marker): number {
    const radius = circle.getRadius(); // in meters
    const circleCenterPoint = circle.getLatLng(); // gets the circle's center latlng
    const isInCircleRadius = Math.abs(circleCenterPoint.distanceTo(marker.getLatLng())) <= radius;
    if (isInCircleRadius === true) {
      return 1 - circleCenterPoint.distanceTo(marker.getLatLng()) / radius;
    } else { return -1; } // NOT WITHIN RADIUS
  }

  private isFacilityBufferIntersectSame(newCircle): boolean {
    if (this.state.storage.circles.length === 0) { return false };
    const c1 = newCircle.getLatLng();
    const r1 = newCircle.getRadius();
    let intersect = false;
    this.state.storage.circles.forEach((circle)=>{
      if (newCircle.options.asset.name === circle.options.asset.name ) {
        const c2 = circle.getLatLng();
        const r2 = circle.getRadius();
        var cDist = c1.distanceTo(c2);
        var rDist = r1 + r2;
        // if center distance is smaller or equals to than sum of radiuses, intersection present
        if (cDist <= rDist) { intersect = true; }
      }
    });
    return intersect;
  }

  private calculateScore(happiness: number, proportions: object, multipliers: object, proximity: number, ldmuMultiplier: number): number {
    let score = 0;
    const ageFilters: string[] = ['adult', 'child', 'elderly'];
    ageFilters.forEach((filter) => { score += happiness * proportions[filter] * multipliers[filter] * proximity; });
    return Math.round(score * ldmuMultiplier);
  }

  private isLatlngInsideSubzone(Latlng): boolean {
    const polyPoints = this.state.subzone.layer._latlngs[0];
    const x = Latlng.lat;
    const y = Latlng.lng;
    let inside = false;
    for (let i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
        const xi = polyPoints[i].lat;
        const yi = polyPoints[i].lng;
        const xj = polyPoints[j].lat;
        const yj = polyPoints[j].lng;
        const intersect = ((yi > y) !== (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) { inside = !inside; }
    }
    return inside;
}

/***************************
   GAMEOVER
****************************/

  async gameOver() {
    // Check satisfy population in the first place
    if (
      // 1 != 1 //testing
      this.state.game.currIncomeFilter.lowIncome < this.gisgameService.getArea().population.incomeFilter.low_income ||
      this.state.game.currIncomeFilter.middleIncome < this.gisgameService.getArea().population.incomeFilter.middle_income ||
      this.state.game.currIncomeFilter.highIncome < this.gisgameService.getArea().population.incomeFilter.high_income
    ) {
      this.fulfillWin = false;
      this.endingMsg = "You failed to house all the population by income group!";
      setTimeout(() => { this.router.navigate(['/dashboard']); }, 3000);
    } else {
      // Check winning condition
      var condition = this.gisgameService.getAttributes().objective_condition;
      var subject = this.gisgameService.getAttributes().objective_subject;
      var sign = this.gisgameService.getAttributes().objective_sign;
      var input = 0;
      switch (subject) {
        case "Happiness":
          input = this.state.game.currScore;
      }
      var satisfied = this.checkSign(input, sign, Number(condition));
      console.log("satisfied: ", satisfied);
      if (satisfied === false) {
        this.fulfillWin = false;
        this.endingMsg = "You failed to satisfy the win condition! (" + this.gisgameService.getAttributes().objective + ")";
        setTimeout(() => { this.router.navigate(['/dashboard']); }, 3000);
      } else if (satisfied === true) {
        this.fulfillWin = true;
        this.endingMsg = "Saving your progress...";
        await this.sendScoreToLeaderboard(this.currentUser.id, this.gisgameService.getAttributes().id, this.state.game.currScore).then((res) => {
          if (res) {
            setTimeout(() => { this.router.navigate(['/dashboard']); }, 3000);
          }
        });
      }
    }
    this.isGameOver = true;
  }

  async sendScoreToLeaderboard(userId: number, gameId: number, score: number) {
    await this.gisgameService.postScore(userId, gameId, score).then((res) => {
      if (res === false) {
        alert("Failed to send score to the server.");
      } else if (res === true) {
        // alert("Score send success.");
        console.log("Score send success");
      }
    });
    return true;
  }

  /***************************
     HELPER
  ****************************/

  public openObjective(): void {
    this.playSoundEffect("open");
    const scope = this;
    if (scope.objectiveOpen) { setTimeout(() => { scope.objectiveOpen = !scope.objectiveOpen; }, 300);
    } else { scope.objectiveOpen = !scope.objectiveOpen; }
  }

  public toggleVideoModal(): void {
    this.showModal = !this.showModal;
  }

  public openPopulationDetail(): void {
    this.playSoundEffect("open");
    var scope = this;
    if (scope.popDetailOpen){ setTimeout(function(){ scope.popDetailOpen = !scope.popDetailOpen; }, 300); }
    else { scope.popDetailOpen = !scope.popDetailOpen;};
  }

  public showHousesBtn(): void { this.isHousesTab = true; this.playSoundEffect("open");}
  public showFacilitiesBtn(): void { this.isHousesTab = false; this.playSoundEffect("open"); }

  public assetHoverAnimation(event): void {
    event.target.classList.add('pulse');
    setTimeout(() => { event.target.classList.remove('pulse'); }, 1000);
  }

  private displayTime(seconds): string {
    const format = val => `0${Math.floor(val)}`.slice(-2);
    const minutes = (seconds % 3600) / 60;
    return [minutes, seconds % 60].map(format).join(':');
  }

  public allowDrop(event): void { event.preventDefault(); }

  private setStatus(status): void {
    this.matSnackBar.open(status, null, {duration: 2000, verticalPosition: 'bottom', horizontalPosition: 'end', });
  }

  public checkSign(input: number, sign: string, condition: number): boolean {
    // console.log(input, sign, condition);
    // return true // testing
    switch (sign) {
      case ">":
        return input > condition;
      case ">=":
        return input >= condition;
      case "<=":
        return input <= condition;
      case "<":
        return input < condition;
      case "==":
        return input == condition;
    }
  }


/***************************
   SFX
****************************/

  private startAudio() {

    // crowd
    this.crowdAudio = new Audio();
    this.crowdAudio.src = "assets/gisgame/audio/crowd.wav";
    this.crowdAudio.load();
    this.crowdAudio.loop = true;
    this.crowdAudio.play();
    this.changeCrowdVolume();

    // bgm
    this.bgmAudio = new Audio();
    this.bgmAudio.src = "assets/gisgame/audio/BGM.ogg";
    this.bgmAudio.load();
    this.bgmAudio.loop = true;
    this.bgmAudio.volume = 0.45;
    this.bgmAudio.play();
  }


  private changeCrowdVolume() {
    if (this.crowdAudio instanceof Audio && this.canPlayAudio == true) {
      this.crowdAudio.volume = this.state.game.currPopulation / this.gisgameService.getArea().population.total_population / 1.7;
    }
  }

  private playSoundEffect(audioName: string) {
    if (this.canPlayAudio == true) {
      let audio = new Audio();
      audio.src = "assets/gisgame/audio/" + audioName + ".ogg";
      audio.load();
      audio.play();
    }
  }

  public toggleSFX() {
    if (this.canPlayAudio == true) {
      this.canPlayAudio = false;
      if (this.crowdAudio instanceof Audio) { this.crowdAudio.pause(); }
      if (this.bgmAudio instanceof Audio) { this.bgmAudio.pause(); }
    } 
    else {
      this.canPlayAudio = true;
      if (this.crowdAudio instanceof Audio) { this.crowdAudio.play(); }
      if (this.bgmAudio instanceof Audio) { this.bgmAudio.play(); }
    }

  }


}
