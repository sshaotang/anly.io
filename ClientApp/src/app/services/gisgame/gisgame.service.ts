import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Area, Attributes, Assets, House, Facility, Gisgame, GisgameScore } from '../../models/gisgame';

@Injectable({
  providedIn: 'root'
})
export class GisgameService {

  private gisgameId = 0;
  private gisgameData: Gisgame = null;
  private area: Area = null;
  private attributes: Attributes = null;
  private assets: Assets = null;

  // ==================================================================

  constructor(private http: HttpClient, private location: Location) {}

  ngOnInit() {}


  async bootGisgame(gameId: number) {
    this.reset();
    const data = await this.http.get<Gisgame>(`${environment.apiUrl}/games/${gameId}`).toPromise();
    try {
      if (!data) {
        alert("No data");
        return false;
      } else {
        if (data) {
          this.gisgameId = gameId;
          this.gisgameData = data;
          this.processGisgame();
          return true;
        }
      }
    } catch (e) {
      alert(e);
      return false;
    }
  }

  private processGisgame() {
      console.log(this.gisgameData);
      this.area = this.gisgameData.game.area;
      this.attributes = this.gisgameData.game.attributes;
      this.assets = this.gisgameData.game.assets;
  }

  public getGisgameData(): Gisgame {
    return this.gisgameData;
  }

  public getArea(): Area {
     return this.area;
  }

  public getAttributes(): Attributes {
    return this.attributes;
  }

  public getAssets(): Assets {
    return this.assets;
  }

  async postScore(userId: number, gameId: number, score: number) {
    const response = await this.http.post<GisgameScore>(`${environment.apiUrl}/score`,{ user: userId, game: gameId, score: score }).toPromise();
    try {
      if (!response) { alert("No response"); console.warn("No response"); return false; }
      else {
        if (response) {
          return true;
        }
      }
    } catch(e) { alert(e); console.warn(e); return false; }
  }

  public reset(): void {
    this.gisgameId = 0;
    this.gisgameData = null;
    this.area = null;
    this.attributes = null;
    this.assets = null;
  }

}
