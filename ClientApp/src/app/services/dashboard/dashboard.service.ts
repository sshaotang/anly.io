import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private data = null;
  private gamesList = null;

  constructor(private http: HttpClient, private location: Location) { }

  async fetchData() {
    this.reset();
    const data = await this.http.get<any>(`${environment.apiUrl}/dashboardgames`).toPromise();
    try {
      if (data === null) {
        alert("ERROR: No data found!");
        return false;
      } else {
        this.data = data;
        // console.log(data);
        this.processData();
        return true;
      }
    } catch (e) {
      console.log(e);
      alert(e);
      return false;
    }
  }

  public processData(): void {
    this.gamesList = this.data;
  }


  public getData(): void {
    return this.data;
  }

  public getGamesList() {
    // console.log(this.gamesList);
    return this.gamesList;
  }

  public reset(): void {
    this.data = null;
    this.gamesList = null;
  }


}
