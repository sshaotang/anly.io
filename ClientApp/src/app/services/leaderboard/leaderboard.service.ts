import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LeaderboardService {

  private gameId = null;

  private data = null;
  private attributes = null;
  private scores = null;

  private selfScore = 0;
  private totalPlayers = 0;
  private playerScore = 0;
  private averageScore = 0;

  constructor(private http: HttpClient, private location: Location) {}

  async fetchData(gameId: number) {
    this.reset();
    const data = await this.http.get<any>(`${environment.apiUrl}/gamescores/${gameId}`).toPromise();
    try {
      if (data === null) {
        alert("ERROR 1: No data found!");
        return false;
      } else {
        this.data = data;
        if (data !== false) { this.processData(); }
        return true;
      }
    } catch (e) {
      console.log(e);
      alert(e);
      return false;
    }
  }

  // async fetchData2(gameId: number, currentUserId: number) {
  //   const data = await this.http.get<any>(`${environment.apiUrl}/gamescores/${gameId}/` + currentUserId).toPromise();
  //   try {
  //     if (!data) {
  //       alert("ERROR 2: No data found!");
  //       return false;
  //     } else {
  //       if (data) {
  //         console.log(data);
  //         if (data.results.game.scores[0]) {
  //           this.selfScore = data.results.game.scores[0].score;
  //         }
  //         return true;
  //       }
  //     }
  //   } catch (e) {
  //     console.warn(e);
  //     alert("ERROR: " + e);
  //     return false;
  //   }
  // }


  private processData() {
      this.attributes = this.data.results.game.attributes;
      this.scores = this.data.results.game.scores;
      this.totalPlayers = this.scores.length;
      this.averageScore = this.calcAverage(this.scores);
  }

  private calcAverage(scores) {
    if (scores.length != 0) {
      var total = 0;
      this.scores.forEach((s)=>{
        total += s.score;
      });
      return total/scores.length;
    } else {
      return 0;
    }
  }

  public getData() {
    return this.data;
  }

  public getAttributes() {
    return this.attributes;
  }

  public getScores() {
    return this.scores;
  }

  public getSelfScore() {
    return this.selfScore;
  }

  public getTotalPlayers() {
    return this.totalPlayers;
  }

  public getAverageScore() {
    return this.averageScore;
  }

  private reset() {
    this.gameId = null;
    this.data = null;
    this.attributes = null;
    this.scores = null;
    this.selfScore = 0;
    this.totalPlayers = 0;
    this.playerScore = 0;
    this.averageScore = 0;
  }



}
