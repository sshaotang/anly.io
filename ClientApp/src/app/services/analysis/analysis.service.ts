import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnalysisService {

  constructor() { }

  public loadGameStatistics() {

  }

  public getGameStatistics() {
    return this.dummyData;
  }

  public 

  public dummyData = {
    e3Index: {
      stuEfficiency: 1403,
      avgEfficiency: 1003,
      stuEffectiveness: 1003,
      avgEffectiveness: 1003,
      stuEquity: 1403,
      avgEquity: 1003,
    },
    happinessPerBudget: 5843758,
    utilizationRate: [
      {
        name: "Clinic",
        data: 80,
      },
      {
        name: "Community Centre",
        data: 70,
      },
      {
        name: "Elderly Care Centre",
        data: 67,
      },
      {
        name: "Food Centre",
        data: 90,
      },
      {
        name: "Gas Station",
        data: 61,
      },
      {
        name: "Mall",
        data: 78,
      },
      {
        name: "Play Ground",
        data: 56,
      },
      {
        name: "School",
        data: 88,
      },
    ],
    facilityAccess: [
      {
        name: "Low",
        data: 8,
      },
      {
        name: "Middle",
        data: 15,
      },
      {
        name: "High",
        data: 12,
      },
    ],
    mixing: [
      {
        name: "Social",
        data: 1889,
      },
      {
        name: "Age Group",
        data: 2456,
      },
    ]
  }

}
