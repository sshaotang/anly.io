import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//
import { AuthGuard } from './helpers/auth.guard';
//
import { DashboardComponent } from './dashboard/dashboard.component'
import { GisgameComponent } from './gisgame/gisgame.component'
import { LoginComponent } from './login/login.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { AnalyticsComponent } from './analytics/analytics.component';

const routes: Routes =
[
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'leaderboard', component: LeaderboardComponent, canActivate: [AuthGuard] },
  { path: 'game/gisgame', component: GisgameComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'analysis', component: AnalyticsComponent, canActivate: [AuthGuard] },
  // otherwise redirect to home
  { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
