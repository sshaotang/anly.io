export interface Area {
  id: number;
  name: string;
  population: any;
}

export interface Attributes {
  description: string;
  id: number;
  money_given: number;
  name: string;
  time_given: number;
  objective_sign: string;
  objective_condition: string;
  objective_subject: string;
  objective: string;
  has_leaderboard: boolean;
  ldmu: number;
}

export interface House {
  cost: number;
  happiness: number;
  id: number;
  name: string;
  size: number;
  type: string;
}

export interface Facility {
  buffer: number;
  cost: number;
  id: number;
  multipliers: any;
  name: string;
  size: number;
  type: string;
}

export interface Assets {
  houses: House[];
  facilities: Facility[];
}

export interface Game {
  area: Area;
  assets: Assets;
  attributes: Attributes;
}

export interface Gisgame {
    game: Game;
}

export interface GisgameScore {
    game: number;
    user: number;
    score: number;
}