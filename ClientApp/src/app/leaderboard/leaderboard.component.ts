import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './../models/user';
import { LeaderboardService } from '../services/leaderboard/leaderboard.service';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  public loaded = false;
  public currentUser = null;

  constructor(
    public leaderboardService: LeaderboardService, 
    public authenticationService: AuthenticationService,
    public router: Router
    ) {
    this.currentUser = this.authenticationService.currentUserValue;
    // console.log(this.currentUser);
    if (this.leaderboardService.getData() == null) {
      this.router.navigate(['/dashboard']);
    } else {
      // console.log(this.leaderboardService.getScores());
    }
  }

  ngOnInit() {

  }


}
