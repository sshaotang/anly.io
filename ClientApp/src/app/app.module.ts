import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ParticlesModule } from 'angular-particle';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TooltipModule } from 'ng2-tooltip-directive';
import { MatSnackBarModule } from '@angular/material';
//
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { NgxEchartsModule } from 'ngx-echarts';
//
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { GisgameComponent } from './gisgame/gisgame.component';
import { ContributionCalendarComponent } from './dashboard/contribution-calendar/contribution-calendar.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { HouseChartComponent } from './house-chart/house-chart.component';
import { FacilityChartComponent } from './facility-chart/facility-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ContributionCalendarComponent,
    LoginComponent,
    GisgameComponent,
    LeaderboardComponent,
    AnalyticsComponent,
    HouseChartComponent,
    FacilityChartComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ParticlesModule,
    LeafletModule.forRoot(),
    DragDropModule,
    TooltipModule,
    MatSnackBarModule,
    NgxEchartsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
