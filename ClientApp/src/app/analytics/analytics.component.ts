import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { GisgameService } from '../services/gisgame/gisgame.service';
import {latLng, latLngBounds, Map, icon, Layer} from 'leaflet';
import { AnalysisService } from '../services/analysis/analysis.service'
// import * as L from 'leaflet';

declare let L;

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  public isHousesTab = true;

  // GEO CLASSES 
  public state = {
    game: {
      currPopulation: 0,
      currScore: 0,
      currMoney: 0,
    },
    track: { id: 0 },
    storage: {
      markers: [],
      circles: [],
      houses: [],
    },
    action: {
      targetAsset: null,
      dragging: false,
      dropEvent: null,
      mId: null,
      mAsset: null,
      mPrevPos: null
    },
    elements: {
      map: null,
      mapElement: null,
    },
    subzone: { layer: null, geometry: null }
  };
  

  private cMarker = L.Marker.extend({ options: { id: null, asset: null }});
  private cCircle = L.Circle.extend({ options: { id: null, asset: null }});

  private streetMaps = L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', 
    { 
      detectRetina: true, 
      opacity: 0.2 
    }
  );

  private layersControl = { 
    baseLayers: { 
      'Street Maps': this.streetMaps, 
    }, 
    overlays: {}
  };
  options = { 
    zoom: 15, 
    center: latLng([ 1.37, 104 ]), 
    zoomControl: false,
    layers: [this.streetMaps],
  };

  
  public analysisData = null;

  constructor(
    private http: HttpClient,
    public gisgameService: GisgameService,
    public analysisSrvice: AnalysisService,
  ) { 
    this.analysisSrvice.loadGameStatistics();
    this.analysisData = this.analysisSrvice.getGameStatistics();
    // this.gis;
  }

  ngOnInit() {
  }

  // LEAFLET FUNCTIONS
  // private initMap(): void {
  //   this.elements.map = L.map('map', this.options);
  // }

  public onMapReady(map: Map):void {
    this.state.elements.map = map;
    this.state.elements.mapElement = document.getElementById('map')
    this.loadGameResult();
  }

  public loadGameResult() {
    this.generateSubzone(this.state.elements.map);
    this.loadGameFacilities();
  }

  private generateSubzone(map): void {
    this.gisgameService.bootGisgame(2).then((res) => {
      const subzoneName = this.gisgameService.getArea().name;
      this.http.get('assets/gisgame/geojson/subzone/' + subzoneName + '.geojson').subscribe((geojson: any) => {
        const szGroup = L.geoJSON(geojson, {style: { color: '#aaa' , weight: 0, fillOpacity: 1 }});
        // console.log(szGroup);
        szGroup.addTo(map);
        this.state.subzone.layer = szGroup._layers[Object.keys(szGroup._layers)[0]];
        this.state.subzone.geometry = this.state.subzone.layer.feature.geometry;
        map.panTo(latLng(
          (this.state.subzone.layer._bounds._northEast.lat + this.state.subzone.layer._bounds._southWest.lat) / 2,
          (this.state.subzone.layer._bounds._northEast.lng + this.state.subzone.layer._bounds._southWest.lng) / 2)
        );
        const ob = this.state.subzone.layer._bounds;
        const nb = latLngBounds(
          latLng(ob._southWest.lat - 0.005, ob._southWest.lng - 0.005),
          latLng(ob._northEast.lat + 0.005, ob._northEast.lng + 0.005)
        );
        map.setMaxBounds(nb);
      });
    });
  }

  public loadGameFacilities() {

  }

  public showHousesBtn(): void { this.isHousesTab = true; }
  public showFacilitiesBtn(): void { this.isHousesTab = false; }
  
}
