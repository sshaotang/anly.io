import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-house-chart',
  templateUrl: './house-chart.component.html',
  styleUrls: ['./house-chart.component.css']
})
export class HouseChartComponent implements OnInit {

  options: any;
  detectEventChanges = true;

  constructor() { }

  ngOnInit() {
    const dataAxis = ['Public Housing', 'Condominium', 'Landed Housing'];
    const data = [24, 5, 3];
    const yMax = 30;
    const dataShadow = [];

    for (let i = 0; i < data.length; i++) {
      dataShadow.push(yMax);
    }

    this.options = {
      title: {
        text: 'House data',
        textStyle: {
          color: '#04c5cd',
        },
      },
      xAxis: {
        data: dataAxis,
        axisLabel: {
          inside: true,
          textStyle: {
            color: '#fff'
          }
        },
        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        },
        z: 10
      },
      yAxis: {
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          textStyle: {
            color: '#999'
          }
        }
      },
      dataZoom: [
        {
          type: 'inside'
        }
      ],
      series: [
        { // For shadow
          type: 'bar',
          itemStyle: {
            normal: { color: 'rgba(0,0,0,0.05)' }
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: dataShadow,
          animation: false
        },
        {
          type: 'bar',
          data: data,
          itemStyle: {
            normal: { color: '#04c5cd' }
          },
        }
      ]
    };
  }

  onChartEvent(event: any, type: string) {
    console.log('chart event:', type, event);
  }
}
