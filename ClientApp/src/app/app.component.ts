import { Component, HostListener } from '@angular/core';
import { Router, NavigationEnd  } from '@angular/router';
import { AuthenticationService } from './services/authentication/authentication.service';
import { User } from './models/user';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  public currentUser: User
  public innerWidth: any;

  public courses: string[] = ['Pin Points on Map', 'Buffering 101', 'Planning your City', 'Maintaining Satisfaction']
  public savedCoursesOpen = false;

  public particleStyle: object = {};
  public particleParams: object = {};
  public particleWidth = 100;
  public particleHeight = 100;

  public pendingRoute = '';


  constructor(
      public router: Router,
      private authenticationService: AuthenticationService,
  ) {
      // window.alert = function() { };
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  public logout() {
      this.authenticationService.logout();
      // this.router.navigate(['/login']);
      window.location.href = 'https://anly.io/landing';
  }

  ngOnInit() {
      this.innerWidth = window.innerWidth;
      this.particleStyle = {
        'position': 'fixed',
        'width': '100%',
        'height': '100%',
        'z-index': -1,
        'top': 0,
        'left': 0,
        'right': 0,
        'bottom': 0,
      };

      this.particleParams = { particles: {
          number: { value: 100, },
          color: { value: '#fff' },
          shape: { type: 'circle', },
          line_linked: { enable: false, }}};

      this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });

  }

  public openSavedCourses() { this.savedCoursesOpen = !this.savedCoursesOpen; }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  public menuRoute(route: string) {
    if (this.router.url.includes('game')) {
      this.pauseGameModal();
      this.pendingRoute = route;
    } else {
      this.router.navigateByUrl(route);
    }
  }

  public pauseGameModal() {
     $("#pauseGameModal").modal({backdrop: 'static', keyboard: false});
     $("#pauseGameModal").modal('show');
  }

  public confirmNavigate() {
    if (this.pendingRoute !== '') {
      this.router.navigateByUrl(this.pendingRoute);
      $("#pauseGameModal").modal('hide');
    }
  }

}
