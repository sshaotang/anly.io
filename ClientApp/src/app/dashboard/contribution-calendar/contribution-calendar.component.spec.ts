import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributionCalendarComponent } from './contribution-calendar.component';

describe('ContributionCalendarComponent', () => {
  let component: ContributionCalendarComponent;
  let fixture: ComponentFixture<ContributionCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributionCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributionCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
