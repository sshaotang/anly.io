import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../models/user';

@Component({
  selector: 'app-contribution-calendar',
  templateUrl: './contribution-calendar.component.html',
  styleUrls: ['./contribution-calendar.component.css']
})

export class ContributionCalendarComponent implements OnInit {
  currentUser: User

  contributionDatas = [
    [0, 0, 0, 30, 40, 50, 30],
    [0, 10, 20, 10, 20, 30, 30],
    [0, 10, 20, 30, 40, 50, 0],
    [0, 10, 20, 30, 10, 20, 0],
    [0, 10, 20, 30, 40, 0, 0]
  ]

  constructor() { }

  ngOnInit() {
  }



  loadLastMonthcontribution() {
    this.contributionDatas = [
      [1, 1, 1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1, 1, 1]
    ]
  }

  loadNextMonthcontribution() {
    this.contributionDatas = [
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
    ]



  }

}
