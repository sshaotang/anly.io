import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { DashboardService } from '../services/dashboard/dashboard.service';
import { GisgameService } from '../services/gisgame/gisgame.service';
import { LeaderboardService } from '../services/leaderboard/leaderboard.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public currentUser = null;
  public loading = true;

  constructor(
    public router: Router,
    private matSnackBar: MatSnackBar,
    public gisgameService: GisgameService,
    public leaderboardService: LeaderboardService,
    public authenticationService: AuthenticationService,
    public dashboardService: DashboardService
  ) {
    this.currentUser = authenticationService.currentUserValue;
    this.loadPage();
  }


  ngOnInit() {
  }


  async loadPage() {
    await this.dashboardService.fetchData().then((res)=>{
      if (res === false) {
        alert('Dashboard failed to load.');
      } else if (res === true) {
        // console.log(this.dashboardService.getGamesList());
        this.loading = false;
      }
    });
  }

  public logout() {
    this.authenticationService.logout();
    window.location.href = 'https://anly.io/landing';
  }

  async gotoLeaderboard(gameId: number, userId: number) {
    if (this.loading === false) {
      this.loading = true;
      await this.leaderboardService.fetchData(gameId).then((res)=>{
        if (res === false) {
          alert("Failed to load!");
          this.loading = false;
        } else {

          // console.log(this.leaderboardService.getData());
          if (this.leaderboardService.getData() === false) { 
            this.loading = false;
            this.setStatus("No leaderboards found!");
          } else {
            this.router.navigate(['leaderboard']);
            // this.leaderboardService.fetchData2(gameId, userId).then((res2) => {
            //   if (res2 === true) {
            //     this.router.navigate(['leaderboard']);
            //   } else {
            //     alert("Failed to load!");
            //     this.loading = false;
            //   }});
          }}});
    }
  }

  async gotoGisgame(gameId: number) {
    if (this.loading === false) {
      this.loading = true;
      await this.gisgameService.bootGisgame(gameId).then((res)=>{
        if (res === true) {
          this.router.navigate(['game/gisgame']);
        } else {
          alert("Failed to load!");
          this.loading = false;
        }});
    }
  }

  // ===========================================================================================================

  private setStatus(status): void {
    this.matSnackBar.open(status, null, { duration: 500, verticalPosition: 'bottom', horizontalPosition: 'end', });
  }

}
