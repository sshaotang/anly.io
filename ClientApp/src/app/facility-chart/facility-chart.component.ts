import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facility-chart',
  templateUrl: './facility-chart.component.html',
  styleUrls: ['./facility-chart.component.css']
})
export class FacilityChartComponent implements OnInit {
  options: any;
  detectEventChanges = true;

  constructor() { }

  ngOnInit() {
    const dataAxis = ['Mall', 'Clinic'];
    const data = [3, 5];
    const yMax = 10;
    const dataShadow = [];

    for (let i = 0; i < data.length; i++) {
      dataShadow.push(yMax);
    }

    this.options = {
      title: {
        text: 'Facility data',
        textStyle: {
          color: '#04c5cd',
        },
      },
      xAxis: {
        data: dataAxis,
        axisLabel: {
          inside: true,
          textStyle: {
            color: '#fff'
          }
        },
        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        },
        z: 10
      },
      yAxis: {
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          textStyle: {
            color: '#999'
          }
        }
      },
      dataZoom: [
        {
          type: 'inside'
        }
      ],
      series: [
        { // For shadow
          type: 'bar',
          itemStyle: {
            normal: { color: 'rgba(0,0,0,0.05)' }
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: dataShadow,
          animation: false
        },
        {
          type: 'bar',
          data: data,
          itemStyle: {
            normal: { color: '#04c5cd' }
          },
        }
      ]
    };
  }

  onChartEvent(event: any, type: string) {
    console.log('chart event:', type, event);
  }
}
