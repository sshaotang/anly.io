/** Anly.io - Priority Scope **/ 
================================

////////////////////

** Core Functions **

# Dashboard Module
------------------
- Lessons (termed "Quests")
- View all available Quests
- Data visualization of basic learning statistics
- View learning "points"
- Ability to favorite and view favorited courses


# Search Quest Module
---------------------
- Search for specific Quest
- Sort by difficulty, topic, etc. 


# Do Quest Module
--------------
- LeetCode-styled format:
- Left-hand side: Quest content, questions, possibly answer fields
- Right-hand side: Interactive and dynamic analytical content (e.g. map visualization, charts, 3D, etc)
- Discussion forum for each quest
- High scores and leaderboards (If Quest involves it)


# Account Module
----------------
- Login/Logout
- Registration
- Profile Management
- Points


/////////////////////////

** Secondary Functions **


# Administrator Module
----------------------
- Visualize basic statistics from user activity
- E.g. popular quests, times of use, 


/////////////////////////

** Tertiary Functions **

# Learning Schedule Module
--------------------------



////////////////////////////

** Good-to-have Functions **


# Points Exchange Module
------------------------





