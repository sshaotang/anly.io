<h1>ANLY.IO - Monolithic Demo Build</h1>

<h3><u>Components</u><h3>

<ul>
    <h4>Build-specific</h4>
    <li>BackEnd - Flask App</li>
    <li>BackEnd/app.db - Flask App SQLite DB; Demo purposes only</li>
    <li>requirements.txt - For Flask App; <code>pip install -r requirements.txt</code></li>
    <li>ClientApp - Angular Files; Not to be copied into built Docker image, but only the built dist folder</li>
    <li>ClientApp/build-angular.sh - Compile and build static files into BackEnd/dist folder</li>
    <li>boot.sh - Docker ENTRYPOINT. Installs python deps and starts gunicorn server</li>
    <li>.gitlab-ci.yml - Specifies config for Gitlab CI/CD</li>
    <li>.eb-config.sh - Specifies AWS deployment credentials during Gitlab Deployment</li>
    <li>Dockerrun.aws.json - Specifying single Docker container configuration for AWS EB</li>
</ul>
<ul>
    <h4>Non Build-specific</h4>
    <li>build-docker.sh - Build a docker image of application (LOCAL TESTING)</li>
    <li>run-docker.sh - Run a docker image of application (LOCAL TESTING)</li>
    <li>docs - Contains miscellaneous project artifacts</li>
</ul>

<hr>

<h3><u>Steps</u><h3>
<ol>
    <h4>Building</h4>
    <li></li>
</ol>
<ol>
    <h4>Local Testing</h4>
    <li></li>
</ol>
<ol>
    <h4>Local Deployment to EB</h4>
    <li></li>
</ol>
<ol>
    <h4>Gitlab Deployment to EB</h4>
    <li></li>
</ol>