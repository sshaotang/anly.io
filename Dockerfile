FROM jonathanabila/python-nodejs:latest

WORKDIR /home/anly-io

COPY requirements.txt requirements.txt

# RUN python3 -m venv venv
RUN pip install -r requirements.txt
# RUN pip uninstall pandas --yes
# RUN pip install pandas

COPY BackEnd/app BackEnd/app
COPY BackEnd/data BackEnd/data
# COPY BackEnd/migrations BackEnd/migrations
COPY BackEnd/app.db BackEnd/app.py BackEnd/config.py BackEnd/

# COPY ClientApp/ ClientApp/

COPY boot.sh ./
RUN chmod +x boot.sh

RUN apt-get update -y
RUN apt-get install dos2unix
RUN dos2unix boot.sh

ENV FLASK_APP app.py

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
